# ESLint

ESLint statically analyzes your code to quickly find problems. ESLint is built into most text editors and you can run ESLint as part of your continuous integration pipeline.

Upstream documentation can be found [here](https://github.com/eslint/eslint#readme).

## Usage

Typical local usage of this image:

Building the image:
Run the following build command from the directory that contains your Dockerfile. Enter a name for your image in the <imageName> placeholder:
```
docker build -t <imageName> .
```

Verify your build has completed successfully and exists:
```
docker images
```

Example Use:
```
docker run -it --entrypoint bash <imageName> (or <ImageID> if no name)
```
Once inside your image:
```
yarn run eslint -v
```
