ARG BASE_REGISTRY=registry1.dso.mil
ARG BASE_IMAGE=ironbank/opensource/nodejs/nodejs14
ARG BASE_TAG=14.17.5

FROM ${BASE_REGISTRY}/${BASE_IMAGE}:${BASE_TAG}

USER root

COPY package.json package.json
COPY yarn.lock yarn.lock
COPY .yarnrc.yml .yarnrc.yml
COPY .yarn .yarn

RUN yarn install

USER node
HEALTHCHECK CMD yarn run eslint -v
ENTRYPOINT ["eslint"]
